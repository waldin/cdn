import networkx as nx
import matplotlib.pyplot as plt
#import closureutils as perms
import math
import copy
'''
 Global variables
 - global clients
 - global temp_edges
 - global pair
 - global server


'''



class CDN_Tree:
   def __init__(self,proxy, clients, server):
       self.proxy = proxy
       self.clients = clients
       self.server = server
       self.tree = self.setup_cdn_tree()
       self.forwarding_nodes = []
       self.edge_nodes = []
       self.non_edge_proxies =[]
       self.all_nodes = set()
       self.u_q = float(0)    #average disk service rate
       self.u_n = float(0)    #content fwding delay B/C
       self.u_n_p = float(0)  #content storing delay(proxy) B/D
       self.control_message = float(0) # bits
       self.data_message = float(0)  #bits
       self.bandwidth = float(0)  # 10Mbps
       self.kd = float(1)
       self.kd_prime=float(1)
       self.kn = float(1)

       #self.forwarding_nodes = self.assign_forwarding_nodes()

   def setup_cdn_tree(self):

       G=nx.DiGraph()

       n=[1,2,3,8,7,9,6,14,5,12,13,18,16,100,200,300,400,500,1000]
       print 'N is ', n
       G.edges(data=True)
       G.add_nodes_from(n)
       ls = int(2)  #Server service rate
       #self.server = 1000

       val = int(0)
       G.node[self.server]["node_type"] = "server"
       G.node[self.server]["ls"] = ls
       G.node[self.server]["clients_served"] = []
       G.node[self.server]["lcx"] = float(0)
       G.node[self.server]["lc"] = float(0)
       G.node[self.server]["ucx"] = float(1)


       for c in self.clients:
            print " C is: ",c
            G.node[c]["node_type"]="client"
            val += int(0)
            G.node[c]["lc"] = val # randomly assigning client request rates initially
            G.node[c]["clients_served"] = []
            G.node[c]["lcx"] = float(0)



        ## Request rate values for tree clients:
       G.node[100]["lc"] = float(5)
       G.node[200]["lc"] = float(4)
       G.node[300]["lc"] = float(3)
       G.node[400]["lc"] = float(3)
       G.node[500]["lc"] = float(1)


       #Prof Tree 2 edges
       e1 = (1,2,1)
       e2 = (2,3,1)
       e3 = (3,300,1)
       e4 = (2,8,1)
       e5 = (8,7,1)
       e6 = (8,9,1)
       e7 = (7,6,1)
       e8 = (6,5,1)
       e9 = (6,12,1)
       e10 = (5,100,1)
       e11 = (12,200,1)
       e12 = (12,13,1)
       e13 = (13,16,1)
       e14 = (16,500,1)
       e15 = (9,14,1)
       e16 = (14,18,1)
       e17 = (18,400,1)
       e18 = (1000,1,1)


       global temp_edges
       temp_edges = [e1,e2,e3,e4,e5,e6,e7,e8,e9,e10,e11,e12,e13,e14,e15,e16,e17,e18]


       G.add_weighted_edges_from(temp_edges)
       print "Number of nodes: ", G.number_of_nodes()

       print "Edges: ", G.number_of_edges()
       print " Server: ", self.server
       print "Nodes are: ", G.nodes()
       print "Edges are: ", G.edges()
       #print 'Node %s neighbors are: %s' %(proxy , G.neighbors(proxy))
       print 'Clients are: ', self.clients

       return G

   def assign_forwarding_nodes(self, H):

       for g in H.nodes(data=True):
            u = str(g)
            if 'node_type'  not in u:
                #print "Testing FOrwWARD Assignment", g
                H.node[g[0]]["node_type"] = "forwarding_node"
                H.node[g[0]]['lc'] = float(0)
       pass



   def get_paths(self,T, clients,proxy):

       ''' Calculates path from clients to proxy/server node

        Returns a list of paths

       '''
       #print "Paths"
       path = []

       proxy_served_nodes = []

       for tget in clients:
           for paths in nx.all_simple_paths(T,source=proxy, target=tget):
               #print paths
               path.append(paths)

               if paths:
                   if tget in paths:
                       #print "Found %s" % tget
                       proxy_served_nodes.append(tget)

       return {'path':path, 'served_clients':proxy_served_nodes}


   def proxy_placement(self,T,clients,pxy):

       ''' performs a recursive call to generate paths from clients to all proxy nodes '''
       lth = int(10000) # used to findshortest path to proxy serving client
       fin_path = []
       t_path = []
       cli_list = []
       count = int(0)
       flow_dict = {} # need for initialising flows
       cl_flow = {}
       self.forwarding_nodes = []

       # **** initialising server parameters
       T.node[self.server]['clients_served'] = []
       T.node[self.server]["node_depth"] = 0
       T.node[self.server]['type'] = ""
       T.node[self.server]['lc']=float(0)
       T.node[self.server]['lcx']=float(0)
       T.node[self.server]['flows'] = flow_dict
       T.node[self.server]['prob_status']=int(0)
       self.all_nodes.add(self.server)

       #print "Incoming Proxy list", pxy
       for prox in pxy:
           t = self.get_paths(T,clients,prox)
           fin_path.extend(t['path'])
           cli_list.extend(t['served_clients'])
           T.node[prox]["node_type"] = "proxy" #REVISIT
           T.node[prox]["node_depth"] = self.calc_tree_depth(T, self.server, prox)
           T.node[prox]['clients_served'] = []
           T.node[prox]['lc']=float(0)
           T.node[prox]['lcx']=float(0)
           T.node[prox]['prob_status']=int(0)
           T.node[prox]['type'] = "" # Here we initialise this variable to determine if a proxy is on the edge directly serving clients
           if prox in self.edge_nodes:
               T.node[prox]['type']='edge'
           else:
               T.node[prox]['type']='non_edge'

           #for c3 in self.clients:
           T.node[prox]['flows'] = flow_dict
           self.all_nodes.add(prox)



       #*************Assigning all remaining nodes to be forwarding nodes*******
       for g in T.nodes(data=True):
            u = str(g)
            if 'node_type'  not in u:
                #print "Testing FOrwWARD Assignment", g
                T.node[g[0]]["node_type"] = "forwarding_node"
                T.node[g[0]]['lc'] = float(0)
                T.node[g[0]]['lcx']=float(0)
                T.node[g[0]]['ucx']=float(1)
                T.node[g[0]]["clients_served"] = []
                T.node[g[0]]['flows'] = flow_dict
                T.node[g[0]]['type'] = "non_edge"
                T.node[g[0]]['prob_status']=int(0)
                if g[0] in self.edge_nodes:
                    T.node[g[0]]['type']='edge'
                T.node[g[0]]["node_depth"] = self.calc_tree_depth(T, self.server, g[0])
                self.forwarding_nodes.append(g[0])
                self.all_nodes.add(g[0])

       #****** initialising client assignments *******************
       print "Clients assignments: "

       for ca in clients:
           T.node[ca]["node_depth"] = self.calc_tree_depth(T, self.server, ca)
           T.node[ca]['flows'] = flow_dict
           T.node[ca]['type'] = "client"
           T.node[ca]['prob_status']=int(0)
           T.node[ca]['lambda_c'] = T.node[ca]['lc']
           cl_flow = {ca:T.node[ca]['lc']}
           T.node[ca]['flows']=cl_flow
           for pths in nx.all_simple_paths(T,source=self.server, target=ca):
               #print "Prinnt prox: ", pths
               f = [p1 for p1 in pths if p1!=ca]
               for f2 in f:
                    T.node[f2]['clients_served'].append(ca)
           self.all_nodes.add(ca)


       ## draw graph
       #pos = nx.draw_spectral(T)
       #nx.draw(T, pos)

       ## show graph
       #plt.show()

       return t_path  # returns the paths served to clients for all proxies

   def calc_tree_depth(self,T, server, client_node):
        '''
        Determines depth of node in Tree

        Parameters:
            - Graph Object T
        Returns:  <depth> type int

        Info: 1) Calculates the depth of the current node in the tree.
                 This is achieved by getting the path to the main server.
                 Then the number of hops are calculated.
       '''
        path = []
        for paths in nx.all_simple_paths(T,source=server, target=client_node):
            path.append(paths)
            print paths
        print "Downstream Depth Path to client is ",client_node
        print "Proxy Node:",client_node
        print "Path:",path

        depth = (int(len(path[0]))-int(1))
        #print " Tree Depth from root to proxy", depth
        #T.node[client_node]["depth"] = depth

        return depth


def get_serving_node(G,T,proxys,client):
    '''
        Description: This function tests if a proxy node exclusively serves a client
        Input:
            - Graph Object G
            - Proxy Nodes type: List of candidate upstream proxy nodes
            - Client
        Returns:
            - Directly Serving Proxy Node
    '''
    serving_node = None
    children = []

    for p in proxys:
        children.append((p,G.successors(p)))

    for c in children:
        print "C: ", c
        if client in c[1]:
            print "Get_serving_node_function Serving Node: ", c[0]
            if G.node[c[0]]["node_type"] == "forwarding_node":
                #print "INSIDE get_serving_node  FUNCTION"
                print "##################FORWARDING NODE FOUND along path: ###",c[0]
                #pred = G.predecessors(c[0])

                #serving_node = go_further_upstream(G,c[0]) # important feature
                serving_node = c[0]
                #print "Predecessor of forwarding node ok: ", serving_node

            elif G.node[c[0]]["node_type"] == "proxy":
                serving_node = c[0]

    #print "Children: ",children
    if serving_node == None:
        serving_node = T.server

    return serving_node


def proxy_sharing(H, pxy):
    '''
    This function returns a boolean if the current proxy is serving one client
    or a series of other clients

    Parameters:
        - proxy_node, Graph Object
    Returns:
        - True / False
    '''
    if pxy:
        if H.node[pxy]["node_type"]=="proxy" or H.node[pxy]["node_type"]=="server" :
            if len(H.successors(pxy))> 1:
                print "Proxy successors", H.successors(pxy)
                print "Sharing"
                return True
            else:
                print "Proxy Not Sharing"
                return False
        else:
            print "Invalid Node type please use a proxy node for test"




def u_c_p_ns(H,T,cli):
    '''
        Probability function to calculate probabilities
        for client if serving proxy node is NOT shared by other clients

        Parameters: Graph H, T (Tree Object), cli (client)

    '''
    s = T.server
    serv_rate = H.node[s]["ls"]
    client_rate = float(0)
    #client_rate = H.node[cli]["lc"]
    for c,l in H.node[cli]["flows"].items():
        client_rate+=l
    print "Client Rate", client_rate
    cprob = float((float(serv_rate))/(float(client_rate)+float(serv_rate)))

    print "Processing node", cli
    print "Probability Rate for client/node:", cprob

    return cprob


def u_c_p_ys(H, T, cli,py):

    '''
        Probability function to calculate probabilities
        for client if serving proxy node is SHARED by other clients

        Parameters: T (Tree Object), cli (client)

    '''
    s = T.server
    client_rate = float(0)
    print "Server Node",s
    lamda_cprime = int(0)
    cprime = float(0)
    serv_rate = H.node[s]["ls"]
    print "Incoming Node test: ",cli
    print "Incoming Proxy test:",py

    for c,l in H.node[cli]["flows"].items():
        client_rate+=l
        print "Client Rate", client_rate

    print "Test flows:"
    print H.node[py]['flows']
    #print H.nodes(data=True)


    for pr,nd in H.node[py]['flows'].items():
        print "Cprime", cprime
        cprime+=nd
    lamda_cprime = cprime - client_rate

    #prel_proxy_list = get_proxies_along_path(H,T.server,cli)
    #p = get_serving_node(H,T,prel_proxy_list, cli)
    #shared_clients = get_proxy_clients(H,T,p)
##
##    print "SHARED CLIENTS", shared_clients
##    if len(shared_clients)>1:
##        for sc in shared_clients:
##            print"incoming client: ",cli
##            if str(sc) != str(cli):
##                print "SC: ", sc
##                print "LC LCLCLCLCLCLCLC: ",H.node[sc]["lc"]
##                lamda_cprime+=H.node[sc]["lc"]
##    else:
##        print "please use a valid shared client"

    print "Lambda Prime xxxx lc`: ", lamda_cprime


    cprob2 = (float(serv_rate)*(float(client_rate)*float(client_rate)))/\
        (((float(client_rate)+float(serv_rate))*(float(client_rate)+float(serv_rate)))*(float(client_rate)+float(lamda_cprime)))



    print "Processing client/node", cli
    print "Probability Rate for client/node shared:", cprob2



    return cprob2

def go_further_upstream(G,prx):
    u = G.predecessors(prx)
    result = None
    print " Incoming proxy", prx
    print " ValUE OF U:", u[0]
    if G.node[u[0]]['node_type']=='forwarding_node':
        print " I have to go upstream this is a forwarding node"
        result = go_further_upstream(G,u[0])
    elif G.node[u[0]]['node_type']=='proxy':
        print "SUcCESS found proxy upstream: ", u[0]
        result = u[0]
    elif G.node[u[0]]['node_type']=='server':
        print "SUcCESS found server upstream: ", u[0]
        result = u[0]

    return result

def get_proxy_clients(H,T, prx):

    #''''''''''''''NEEDS TO BE FIXED ''''''''''''''''''''''''

    '''
    This function assists the probability function
    by determining all clients served by any proxy node

    Parameters: prx (proxy node)

    Example: MATRIX {'11': [14], '10': [12, 13], '4': [8, 7]}

    Returns a Dictionary with Proxy node and clients served

    '''
    print "Entered get_proxy_clients() function"
    pxy_client_matrix = dict()
    p_list = []
    py = T.proxy
    py.append(T.server)
    # [ py.append(T.server) ] this is just to add the server to the list worst case a client
    #is directly connected to the server through a series of forwarding nodes
    for item in py:
        #print "Current Proxy: ", item
        suc = H.successors(item)
        #print " Children : ", suc
        p_list.extend(suc)

            #print "PLIST CONTENTS", p_list

        if len(p_list) > int(0):
            pxy_client_matrix[str(item)]= p_list
        p_list = []


    #print"Proxy client MATRIX",pxy_client_matrix
    clients_shared = []
    if prx:
        clients_shared = pxy_client_matrix[str(prx)]
        #print "TEST Prox: ", prx
    return clients_shared

################################################################################
def update_flow(H,T,fn):
    '''This function updates the intermediate forwarding nodes with flow values '''
    children = H.successors(fn)
    #if H.node[fn]['node_type']=='forwarding_node':
    if children:
        flow_one = {}
        flow_two = {}
        print "forwarding node:", fn
        for c in children:
            print "Child:",c
            print "Child Node:",H.node[c]
            for k,v in H.node[c]['flows'].items():
                #H.node[fn]['flows'][k]=v
                flow_two[k]=v

        H.node[fn]['flows']=flow_two
        print "Proxy:",fn," was updated to:",H.node[fn]
        #flow_two.clear

    pass

################################################################################
def update_flow_proxy_prob(H,T,pnx, pull_nodes):
    '''This function updates the parent proxy nodes with flow values '''
    p_srv = pull_nodes

    if p_srv:
        w_one = {}
        w_two = {}
        print "Parent node:", pnx
        for xn in p_srv:
            print "updating child:",xn
            print "Child Node to be updated:",H.node[xn]
            for k,v in H.node[xn]['flows'].items():
                w_two[k]=v
                #H.node[pnx]['flows'][k]=v
        H.node[pnx]['flows']=w_two
        print "Proxy:",pnx," was updated to:",H.node[pnx]
        #flow_two.clear

    # We also update the upstream proxies above this node

    up_pxy = d_x_c(H,T,pnx,T.server)

    if pnx != T.server:
        flow_three = {}
        for m,nodez in H.node[pnx]['flows'].items():
            print 'Updating upstream proxy',up_pxy
            #flow_three[m]=nodez
            H.node[up_pxy]['flows'][m]=nodez
        print "Upstream PXY updated also:",up_pxy
        print H.node[up_pxy]

    pass


def update_fwd_nodes_after_prob(H,T):
    '''This function updates all intermediate forwarding nodes each time a probability
       calculation is performed
    '''
    print" forwardin nodes",T.forwarding_nodes
    fw=[T.calc_tree_depth(H,T.server,w) for w in T.forwarding_nodes]
    pw =[T.calc_tree_depth(H,T.server,w) for w in T.proxy]

    print "Fwd_node update levels: ", fw
    fw_level = max(fw)
    pw_level = max(pw)

    print "Max fwd_node update Level", fw_level
    print "Max proxy update level", pw_level
    #### Pulling fwding flows from clients #########
    while fw_level>0:
        for tf in T.forwarding_nodes:
            if H.node[tf]['node_depth'] == fw_level:
                update_flow(H,T,tf)
        fw_level = fw_level -1
##    while pw_level>0:
##        for pws in T.proxy:
##            if H.node[pws]['node_depth'] == pw_level:
##                update_flow(H,T,pws)
##        pw_level = pw_level -1

    pass



def update_flow_clients(H,T):
    print" forwardin nodes",T.forwarding_nodes
    cy=[T.calc_tree_depth(H,T.server,w) for w in T.forwarding_nodes]
    pyl = [T.calc_tree_depth(H,T.server,w) for w in T.proxy]
    print "fwd_node level: ", cy
    c_level = max(cy)
    m_level = max(cy)
    p_level = max(pyl)
    p_level2 = max(pyl)
    print "Max fwd_node Level", c_level
    #### Stage 1: Pulling fwding flows from clients #########
    while c_level>0:
        for ci in T.forwarding_nodes:
            if H.node[ci]['node_depth'] == c_level:
                update_flow(H,T,ci)
        c_level = c_level -1

    #### Stage 2: Pulling proxy flows from fwding nodes and clients  #####
    print 'Proxies',T.proxy
    print 'PLevel', p_level
    while p_level > 0:

        for prxy in T.proxy:
            pl_one = {}
            if H.node[prxy]['node_depth']==p_level:
                print "Proxy:",prxy
                #update_flow(H,T,prxy)
                su = H.successors(prxy)
                for u in su:
                    #if H.node[u]['node_type']=='forwarding_node':
                    #    fu = H.successors(u):
                    for a,b in H.node[u]['flows'].items():
                        pl_one[a]=b
                H.node[prxy]['flows']=pl_one
            #pl_one.clear()
        p_level = p_level -1
    #### Stage 3: Pulling fwding nodes flows form proxy nodes upstream
    while m_level>0:
        for ci in T.forwarding_nodes:
            if H.node[ci]['node_depth'] == m_level:
                update_flow(H,T,ci)
        m_level = m_level -1

    #### Stage 4: Pulling all proxy successors
    while p_level2>0:
        for p2 in T.proxy:
            if H.node[p2]['node_depth']==p_level2:
                update_flow(H,T,p2)
        p_level2 = p_level2 -1

    ## Finally server pulls
    dr = H.successors(T.server)
    srv_one = {}
    for r in dr: # directly connected nodes to server
        for x,y in H.node[r]['flows'].items():
            srv_one[x]=y
        H.node[T.server]['flows']=srv_one

    ##Update current request rates

    for a_n in T.all_nodes:
        print '*********All node ****', a_n
        H.node[a_n]['lc']= sum_flows(H,T,a_n)

    pass
################################################################################
def update_upstream_proxies(H,T,pxv):
    ''' After each client or sub-level node prob calculation is done
        this function recursively updates all upstream clients
    '''
    print "Incoming pxv:",pxv
    count = int(0)
    upstream = d_x_c(H,T,pxv,T.server)
    print "***UPSTREAM: ",upstream
    if upstream != None:
        update_flow(H,T,upstream)
        count+=1
    print" cOUNT",count

##    while upstream != None:
##        update_flow(H,T,upstream)
##        #update_upstream_proxies(H,T,upstream)


    pass


################################################################################
def sum_flows(H,T,hub):
    ''' Sum any flows input '''
    req_rate = float(0)
    for i,j in H.node[hub]['flows'].items():
        req_rate+=j
    return req_rate

def edit_flows(H,T,ndx,ucx_p):


    #H.node[ndx]['node_type']!= 'client':
    for k,l in H.node[ndx]['flows'].items():
        H.node[ndx]['flows'][k]=l*H.node[ndx]['ucx']
##        #H.node[ndx]['lc']+=temp_lc
    ##if H.node[ndx]['node_type'] != 'client':
    sum_reqrate(H,T,ndx)

    pass

def sum_reqrate(H,T,nds):

    H.node[nds]['lc'] = sum_flows(H,T,nds)

    pass

################################################################################

def calc_proxy_prob(H, T, prx):
    '''
        This function calculates the probability values for a single proxy.
        Parameters: Graph H, T (Tree Object), prx

    '''
    print "Calc Proxy Probability function"
    if prx:
        #if H.node[prx]['node_type'] == "proxy"or H.node[prx]['node_type'] == "server":
        if H.node[prx]['node_type'] != "client":
            ucpys = float(0)
            ucpns = float(0)
            lcx = float(0)
            print "*********Calc Proxy Probability function proxy found: ", prx

            temp_proxy_list = get_proxies_along_path(H,T.server,prx)
            p = get_serving_node(H,T,temp_proxy_list, prx)
            print " Serving proxy for this client/node is : ", p
            #determining of proxy node is sharing service or not
            if H.node[p]['node_type']=='forwarding_node':
                #test
                label = "uc_"+str(prx)+"_"+str(p)
                print "Current forwarded lc is : ", H.node[prx]['lc']
                H.node[prx]['lcx'] = H.node[prx]['lc']
                H.node[prx]['ucx'] = float(1)
                H.node[prx][label] = H.node[prx]['ucx']
                #return float(1)
            else:
                if proxy_sharing(H, pxy=p):
                    #print "Shared yessss"
                    label = "uc_"+str(prx)+"_"+str(p)
                    ucpys = u_c_p_ys(H, T, cli=prx)
                    print "Current lc is : ", H.node[prx]['lc']
                    print " ucpys: " , ucpys
                    H.node[prx][label] = ucpys
                    lcxs = ucpys*float(H.node[prx]['lc'])
                    H.node[prx]['lcx'] = lcxs
                    H.node[prx]['ucx'] = ucpys
                    print "New lcx", lcxs
                    #H.node[p]['flows'][0].update({t:)
                    return ucpys

                else:
                    print" not shared by any lol"
                    label2 = "uc_"+str(prx)+"_"+str(p)
                    ucpns = u_c_p_ns(H, T, cli=prx)
                    lcxs = ucpns*float(H.node[prx]['lc'])
                    H.node[prx][label2] = ucpns
                    H.node[prx]['ucx'] = ucpns
                    H.node[prx]['lcx'] = lcxs
                    nxs = (prx,lcxs)
                    #    H.node[p]['flows'].update({t:new_lcx})
                    return ucpns
        #else: #means this is a forwarding node
        #    print "This means this is a forwarding node processing"
        #    H.node[prx]['ucx'] = float(1)
        #    s = H.node[prx]['clients_served']
        #    #for c in s:
        #
        #    H.node[prx]['lcx'] = H.node[prx]['lc']
        #    print "Stored proxy/fwd node:",prx," Lc: ",H.node[prx]['lc']

################################################################################



def server_consolidation(H,T):
    ''' This function retrieves the flows from the proxies at the lower level of the trees

    '''
    final_flow={}
    final_request = float(0)

    s = H.successors(T.server)
    temp_list4 = []

    for dc_node in s:
        print "Calculate non-sharing client probability for node:",dc_node
        H.node[dc_node]['ucx'] = u_c_p_ns(H,T,dc_node)
        H.node[dc_node]['lcx'] = H.node[dc_node]['ucx'] * sum_flows(H,T,dc_node)
        H.node[dc_node]['prob_status']=int(1)

        temp_list4.append(dc_node)
        print "Editing flows for:",dc_node
        edit_flows(H,T,dc_node,H.node[dc_node]['ucx'])
        update_flow_proxy_prob(H,T,dc_node,temp_list4)

    #update_upstream_proxies(H,T,q)
    print "Stored Server:",T.server ,"new flow is:",H.node[T.server]['flows']
    for ts in s:
        print "Stored client:",ts,"is:",H.node[ts]

    print"Finished Prob for Server Connection"

    print "Updating intermediate forwarding nodes"
    update_fwd_nodes_after_prob(H,T)

    print "Updating lc/lamda values"
    for final_node in T.all_nodes:
        if H.node[final_node]['node_type']!='client':
            leaves = H.successors(final_node)
            temp_lc = float(0)
            for l in leaves:
                temp_lc += sum_flows(H,T,l)
            H.node[final_node]['lc'] = temp_lc

    pass



def c_x(H,T,ndx):
    ''' This function returns the list of clients served through node n
        Input node:ndx
        Output list: served
    '''
    print "T.server;",T.server
    t = T.server
    served = []
    for ca in T.clients:
        for pths in nx.all_simple_paths(H,source=T.server, target=ca):
            print "Print CX: ", pths
            if ndx in pths:
                served.append(ca)
            #f = [p1 for p1 in pths if p1!=ca]
            #for f2 in f:
            #    T.node[f2]['clients_served'].append(ca)
    print "Served:",served
    return served
    #pass

def d_x_c(H,T,c,x):
    '''
    Function to determine serving proxy node for a client
    Input, x: proxy node , c: client/node

    '''
    print "Inside d(x,c) function determine serving proxy node"
    xval = None
    #for paths in nx.all_simple_paths(H,source=x, target=c):
    #    print paths
    print "Node is: ",c
    #print "Proxy/Node check is:",x
    xval = serving_proxy(H,T,c)
    print "Result d_x_c", xval
    return xval

def d_x(H,T,root_pxy):
    ''' This function returns list of immediate downstream proxies for a node
        Example D(x): returns a list of immediate downstream proxy nodes
                D(1) : [2,3]

     '''
    print "Inside D(x) served proxies function"
    pxy_list = None
    #print "Clients Served", H.node[root_pxy]['clients_served']
    targets = H.node[root_pxy]['clients_served']
    output = set()


    for t in targets:
        #print "Current client being checked: ", t
        paths = nx.all_simple_paths(H,source=root_pxy, target=t)
        #print "Path: ",list(paths)
        for p in paths:
            #print "P:",p
            t_counter = []
            xpath2 = []
            test = []
            for items in p:
                xpath2 = []
                if H.node[items]['node_type']=='proxy':
                    xpath = nx.all_simple_paths(H,source=root_pxy,target=items)
                    #xpath2 = list(xpath)
                    for ele in xpath:
                        xpath2.extend(ele)
                        #print "New path:",ele,"New proxy: ",ele[-1]
                        test.append(ele[-1])
                        #print "No of Hops : ",len(ele)-1
                        t_counter.append(len(ele)-1)
                        #print "T counter", t_counter
                    if t_counter:
                        min_val = min(t_counter)
                        #print "Min t counter", min_val
                        #print "Min_val", min_val
                        #print "Current state of xpath2",xpath2
                        #print "Proxy found here:",xpath2[min_val]
                        output.add(xpath2[min_val])


    pxy_list = list(output)
    if pxy_list == []:
        pxy_list = None
    print "D(x):",root_pxy,"Served Proxies:",pxy_list

    return pxy_list

def p_a_b_c(H,T,src,dest,cli):

    ''' This function returns all intermediate nodes with shared capacities/branches
        between two nodes.
        Input: src/lower level node | dest: Upper level node |cli: client
    '''
    testpath = nx.all_simple_paths(H,source=dest,target=src)
    #print "Testpath: "
    t_p = []
    result_nodes=[]
    for t in testpath:
        t_p.extend(t)
    print t_p
    if t_p != []:
        del t_p[0]
        del t_p[-1]
        for tp in t_p:
            if H.degree(tp)>2:
                result_nodes.append(tp)

    if result_nodes == []:
        result_nodes = None
    print "Resulting forwarding nodes with shared branches:",result_nodes

    return result_nodes

def serving_proxy(H,T,nd):
    ''' Recursively goes upstream and finds the closest serving proxy node
        Input: nd: node
    '''
    xp = None
    #print "nd::::",nd
    if nd != T.server:
        par = H.predecessors(nd)
        #for p in par:
            #print "Predecessor is", par
        #print "Inside Serving_proxy function"
        if H.node[par[0]]['node_type']== 'proxy':
            xp = par[0]
            #print "Proxy found:",xp
        elif H.node[par[0]]['node_type']== 'server':
            #print "Server check"
            print "Server found:", par[0]
            xp = T.server
        else:
            #print "Recursion called on: ", par[0]
            xp = serving_proxy(H,T,par[0])

    return xp

def served_proxies(H,T,dwn):
    ''' Recursively goes downstream and finds the closest serving proxy node
        Input: dwn: node
    '''
    xy = d_x(H,T,dwn)


    return xy



def proxy_pruning(H,T):
    '''
        This function calculates recursively from the max depth of the tree
        upwards to the server the probability values

    '''
    ty=[T.calc_tree_depth(H,T.server,w) for w in T.proxy if H.node[w]['node_type']=='proxy' ]
    print "TY: ", ty
    level = max(ty)
    print "Max Level", level
    print "Proxy list", T.proxy

    ct = int(0)
    while level>0:
        ct = ct+1
        print "********************************* Round: ", ct
        for n in T.proxy:
            if H.node[n]['node_depth']==level and H.node[n]['node_type']=='proxy':
                print "Level:", level,  "proxy:",n
                served_nodes = H.node[n]['clients_served']
                srv2 = d_x(H,T,n)
                if srv2 !=None:
                    served_nodes.extend(srv2) #adding immediate downstream proxies to list
                print "Served Nodes:",served_nodes
                l_count = int(0)
                #f = [p1 for p1 in pths if p1!=ca]
                srvd = [x for x in served_nodes if H.node[x]['prob_status']==int(0)]
                len_ser = len(srvd)
                temp_list1 = []
                print "***********************************NODES TO BE SERVICED:",srvd,"for proxy:",n
                for s in served_nodes:
                    q= d_x_c(H,T,s,n)
                    if q != None and H.node[s]['prob_status']==0:

                        if proxy_sharing(H, pxy=q) or p_a_b_c(H,T,s,q,s):
                            print "Calculate sharing client probability for node:",s
                            l_count+=1
                            print"L_count:",l_count
                            H.node[s]['ucx'] = u_c_p_ys(H,T,s,q)
                            H.node[s]['lcx'] = H.node[s]['ucx'] * sum_flows(H,T,s)
                            H.node[s]['prob_status']=int(1)
                            temp_list1.append(s)
                            if l_count == len_ser:
                                for s2 in temp_list1:
                                    print "Editing flows for:",s2
                                    edit_flows(H,T,s2,H.node[s2]['ucx'])
                                update_flow_proxy_prob(H,T,n,temp_list1)

                                    #update_flow(H,T,q)
                                    #update_fwd_nodes_after_prob(H,T)

                                #update_upstream_proxies(H,T,q)
                                print "Completed sharing proxy",q, "updates"

                        else:
                            print "Calculate non-sharing client probability for node:",s
                            H.node[s]['ucx'] = u_c_p_ns(H,T,s)
                            H.node[s]['lcx'] = H.node[s]['ucx'] * sum_flows(H,T,s)
                            H.node[s]['prob_status']=int(1)
                            temp_list2 = []
                            temp_list2.append(s)
                            print "Editing flows for:",s
                            edit_flows(H,T,s,H.node[s]['ucx'])

                            #update_flow(H,T,q) #TEST
                            #update_fwd_nodes_after_prob(H,T)
                            update_flow_proxy_prob(H,T,n,temp_list2)

                            #update_upstream_proxies(H,T,q)
                            print "Stored prxy:",q ,"new flow is:",H.node[q]['flows']
                            print "Stored client:",s,"is:",H.node[q]
                print"Finished Prob for branch"
                    #calc_client_prob(H,T,n,s)
            #print "level check",level
        #update_fwd_nodes_after_prob(H,T)
        level = level - 1
        server_consolidation(H,T)
    for h in H.nodes(data=True):print h
    pass

def l_a_b_c(H,T,src_id,dest_id,cl_id):
    ''' This function calculates the link sharing latency manifestation\
        we traverse upstream to each serving proxy calucating the latency
        between forwardign nodes. We also check for intermediate sharing \
        forwarding nodes
        Input: client_id: the client latency being calculated
                src_id: source node downstream
                dest_id: next upstream serving pxy

    '''
    if src_id:
        parent_pxy = d_x_c(H,T,src_id,T.server)
        if parent_pxy == dest_id:
            print "Serving proxy for latency calculation is:",parent_pxy
            t_path = nx.all_simple_paths(H,source=parent_pxy,target=src_id)
            #print "Testpath: ",t_path
            t_p = []
            result_nodes=[]
            for a in t_path:
                t_p.extend(a)
            print t_p
            if t_p != []:
                #del t_p[0]
                del t_p[-1]
            print t_p
            sum_val = float(0)

            intermediate = p_a_b_c(H,T,src_id,parent_pxy,200)


            cnt = int(0)
            print " Intermediate forwarding nodes before:",intermediate
            if intermediate!=None:
                intermediate.reverse()
                intermediate.append(parent_pxy)
                print 'Current state intermediate after',intermediate

                idx_len = len(intermediate)
                start_node = src_id
                for inter_node in intermediate:
                    cnt = cnt +1
                    print "iteration node:", start_node," inter node:",inter_node
                    lzxc = h_a_b_c(H,T,start_node,inter_node)/(T.u_n_p-(H.node[inter_node]['lc']-H.node[start_node]['flows'][cl_id]))
                    print "LZXC:",lzxc
                    #print "Client flow val:",H.node[start_node]['flows'][cl_id]
                    sum_val= sum_val + lzxc

                    start_node = inter_node
                print "Sum val",sum_val
                return sum_val
            else:

                print 'Implement recursive termination here'
                print "iteration node:", src_id," dest node:",dest_id
                lzxc = h_a_b_c(H,T,src_id,dest_id)/(T.u_n_p-(H.node[dest_id]['lc']-H.node[src_id]['flows'][cl_id]))
                print "Client flow val:",H.node[src_id]['flows'][cl_id]
                print "LZXC:",lzxc
                return lzxc

        else:
            print "Incorrect hop destination in l_a_b_c function"

    pass

def get_segments(H,T,src_nd,dest_nd):
    segs = []
    #return sum_val
    print "Part two test getting segments"
    ups = d_x_c(H,T,src_nd,T.server)
    print "Upstream Node",ups
    up_list = []
    while H.node[ups]['node_type']!='server':
        up_list.append(ups)
        ups = d_x_c(H,T,ups,T.server)
    print "Upstream list",up_list
    start_nd = src_nd
    for u in up_list:
        segs.append((start_nd,u))
        start_nd = u
    print "Segments",segs

    return segs

def l_cl_yt_c_r(H,T,edge_client,srv_proxy):
    ''' This function calculates the latency for a client connected to a serving\
        proxy node in addition to any residual latencies experienced upstream
        Input: edge_client - client/customer node <type> int
                srv_proxy - serving proxy for this node <type> int

    '''
    #up_segments = get_segments(H,T,edge_client,srv_proxy)
    #term_1 = 5
    #term_2 = 7

    # Delete first hop of the segment client to proxy to the remaining latecny\
    # can be calculated
    #del up_segments[0]

    i_client_px = d_x_c(H,T,edge_client,T.server) # getting the serving proxy for the client
    requesting_client = edge_client
    t_result = l_prime_cl_yt_r_x(H,T,i_client_px,requesting_client,0)

    term_2 = (h_a_b_c(H,T,att(H,T,requesting_client),i_client_px)) * ((float(1) /(T.u_n-H.node[requesting_client]['lc'])) + (float(1)/(T.u_n_p - H.node[requesting_client]['lc'] ) ))
    latency_cl_yt_c_r = (float(1)/(T.u_q-summation_lc_prime(H,T,requesting_client,i_client_px))) + term_2

    print "latency_cl_yt_c_r ", latency_cl_yt_c_r
    #print "T result:", t_result
    #print 'Term 2',term_2

    lt_result = latency_cl_yt_c_r + t_result
    H.node[requesting_client]['lt_cl_yt'] = lt_result
    print "Latency Result",lt_result

    pass

def l_prime_cl_yt_r_x(H,T,e_client_src,r_client,na):

    ''' r_client: this is the client that the latency is being calculated for
        e_client_src: is the next hop serving proxy
    '''
    r_client_temp = r_client
    print "Entered*********************************************"
    print 'Static client:',r_client_temp
    print 'incoming client:', e_client_src
    #values = int(0)
    s_node = d_x_c(H,T,e_client_src,T.server)
    print "Found Node:",s_node

    if s_node == T.server:
        #print "Entered exit clause"
        #print "ECLIENT src:",e_client_src,"EDEST:",s_node
        #print "ECLIENT LCX:", H.node[e_client_src]['lcx']
        #print "ECLIENT Server rate: ",H.node[T.server]['ls']
        #print "ECLIENT u_q:",T.u_q

        new_val= float(1)/(T.u_q-(H.node[T.server]['ls'])-(H.node[e_client_src]['lcx'] - H.node[e_client_src]['flows'][r_client_temp]))

        #print "newval:",new_val

        return new_val
    else:
        #print "NA before:",na
        #print "ECLIENT src:",e_client_src,"EDEST:",s_node

        l_term1 = float(1)/(T.u_q-((1-H.node[s_node]['ucx'])*H.node[s_node]['lc']))
        l_term2 = l_a_b_c(H,T,e_client_src,s_node,r_client_temp)
        t_hops = h_a_b_c(H,T,e_client_src,s_node)
        l_term3 = t_hops*((float(1)/(T.u_n_p-H.node[s_node]['lc']))+(float(1)/(T.u_n-H.node[T.server]['ls'])))
        l_term4 = float(1)/(T.u_q-H.node[s_node]['lcx'])
        print "l term",l_term1, "L term 2:",l_term2, "l_term3:",l_term3

        na=l_term1 + l_term2 + l_term3+ (H.node[s_node]['ucx'] *l_prime_cl_yt_r_x(H,T,s_node,r_client_temp,0)) +l_term4

        #print "NA after:",na
        return na

############################################################################################

def l_cl_nt_c_r(H,T,n_client,sr_proxy):
    ''' This function calculates the latency CL_NT for a client connected to a serving\
        proxy node in addition to any residual latencies experienced upstream
        Input: n_client - client/customer node <type> int
                sr_proxy - serving proxy for this node <type> int
        edge_client,srv_proxy
    '''
    next_hop_px = d_x_c(H,T,n_client,T.server) # getting the serving proxy for the client
    req_client = n_client
    n_result = l_prime_cl_nt_r_x(H,T,next_hop_px,req_client,0)

    nterm_2 = (h_a_b_c(H,T,att(H,T,req_client),next_hop_px)) * ((float(1) /(T.u_n-H.node[req_client]['lc'])) + (float(1)/(T.u_n_p - H.node[req_client]['lc'])))

    latency_cl_nt_c_r = (float(1)/(T.u_q-summation_lc_prime(H,T,req_client,next_hop_px))) + nterm_2

    print "latency_cl_nt_c_r NT: ", latency_cl_nt_c_r
    #print "N result nt:", n_result
    #print 'NTerm 2 nt',nterm_2

    lt_nt_result = latency_cl_nt_c_r + n_result
    H.node[req_client]['lt_cl_nt'] = lt_nt_result
    print "Latency Result CL_NT",lt_nt_result

    pass


def l_prime_cl_nt_r_x(H,T,inc_src,r_client,na):
    ####  LT_nt
    ''' r_client: this is the client that the latency is being calculated for
        inc_src: is the next hop serving proxy
    '''
    stat_client= r_client
    #print "Entered*********************************************"
    #print 'Static client:',stat_client
    #print 'incoming client:', inc_src
    #values = int(0)
    next_hop = d_x_c(H,T,inc_src,T.server)
    print "Found Node:",next_hop

    if next_hop == T.server:
        #print "Entered exit clause NT function"
        #print "ECLIENT NT src:",inc_src,"EDEST:",next_hop
        #print "ECLIENT NT LCX:", H.node[inc_src]['lcx']
        #print "ECLIENT NT Server rate: ",H.node[T.server]['ls']
        #print "ECLIENT NT u_q:",T.u_q

        new_val_nt= float(1)/(T.u_q-(H.node[T.server]['ls'])-(H.node[inc_src]['lcx'] - H.node[inc_src]['flows'][stat_client]))

        #print "newval:",new_val_nt

        return new_val_nt
    else:
        #print "NA before NT :",na
        #print "ECLIENT NT src:",inc_src,"EDEST:",next_hop

        l_term1_nt = float(1)/(T.u_q-((1-H.node[next_hop]['ucx'])*H.node[next_hop]['lc']))
        l_term2_nt = l_a_b_c(H,T,inc_src,next_hop,stat_client)
        t_hops_nt = h_a_b_c(H,T,inc_src,next_hop)

        #This equation is the difference between the latency yt and nt schemes
        l_term3_nt = t_hops_nt*((float(1)/(T.u_n_p-H.node[next_hop]['lc']))+(float(1)/(T.u_n-( (1-H.node[inc_src]['ucx']) * H.node[inc_src]['lc'] ))))

        l_term4_nt = float(1)/(T.u_q-H.node[next_hop]['lcx'])
        #print "l term nt",l_term1_nt
        #print "L term 2 nt:",l_term2_nt
        #print "l_term3 nt:",l_term3_nt
        #print "T hops:", t_hops

        na_nt=l_term1_nt + l_term2_nt + l_term3_nt+ (H.node[next_hop]['ucx'] *l_prime_cl_yt_r_x(H,T,next_hop,stat_client,0)) +l_term4_nt

        #print "NA after:",na_nt
        return na_nt


##########  Latency SR Calculation ############################################

def latency_sr(H,T,sr_client):
    ''' This function calculates the latency SR Server driven value for a client
        connected to a serving proxy node
        Input: sr_client - client/customer node <type> int
                lsr_proxy - serving proxy for this node <type> int
        edge_client,srv_proxy
    '''
    sum_rates = float(0)
    srving_px = d_x_c(H,T,sr_client,T.server) # getting the serving proxy for the client
    no_of_hops = h_a_b_c(H,T,sr_client,srving_px)
    lsr_l_a_b_c = l_a_b_c(H,T,sr_client,srving_px,sr_client)

    if no_of_hops == 1:
        for k,v in H.node[srving_px]['flows'].items():
            print 'node',k
            sum_rates +=H.node[k]['lambda_c']
        #print "Rates:",sum_rates
    else:
        chil = H.successors(srving_px)
        for ch in chil:
            sum_rates+=H.node[ch]['lc']
        #print "Rates more than two hops:",sum_rates

    lsr_term1 = float(1)/(T.u_q - (H.node[T.server]['ls'] + sum_rates ))

    #lsr_term3 = (h_a_b_c(H,T,sr_client,srving_px))/(T.u_n_p - H.node[srving_px]['lc'] )
    lsr_term3 = (no_of_hops)/(T.u_n_p - sum_rates )
    latency_sr_value = lsr_term1 + lsr_l_a_b_c + lsr_term3
    #print 'Serving px sr scheme',srving_px
    #print "U n double prime", T.u_n_p
    #print "Uq:",T.u_q

    #print "term 3:",lsr_term3

    print "latency sr for client", sr_client, "is :",latency_sr_value
    H.node[sr_client]['lt_sr'] = latency_sr_value
    #print "Latency Result SR Scheme",lt_nt_result

    pass

def summation_lc_prime(H,T,c1,p1):
    ''' this function calculates the summation prime of all other clients connected to the proxy
        using equation 12 first term (1/uq-Summation(lc*(1+ucx)
        Input : client and serving proxy
    '''
    sum_res = float(0)
    for cs,va in H.node[p1]['flows'].items():
        if cs!=c1:  # Taking the sum of all othre clients
            #print "Key:",cs," val:",va
            sum_res = sum_res + ((H.node[cs]['lc'])*(1+H.node[cs]['ucx']))
            #print "Sum res:",sum_res
    return sum_res


def h_a_b_c(H,T,nd_a,nd_b):
    ''' This function calculates the number of hops between two nodes if a path exists
        Input: nd_a : start node type(int)
               nd_b : end node type(int)
    '''
    hops = int(1)
    h_list = []
    h_path = nx.all_simple_paths(H,source=nd_b,target=nd_a)

    if h_path:
        for h in h_path:
            h_list.extend(h)


        hops = len(h_list)-int(1)
    if hops == -1:
        hops = 0

    print 'Hops path:',hops
    #pass
    return hops

def att(H,T,cx):
    '''Function returns directly connected node in the upstream path '''
    direct_node = None
    direct_node = H.predecessors(cx)
    print "Att(x):",cx,":",direct_node
    return direct_node[0]


################## Overhead Calculations ###################################

def o_cl_yt_c_r(H,T,o_client,sr_proxy):
    ''' This function calculates the overhead
    '''
    next_hop_px = d_x_c(H,T,o_client,T.server) # getting the serving proxy for the client
    sum_lc_prime = float(0)
    o_hops = h_a_b_c(H,T,att(H,T,o_client),next_hop_px)
    if o_hops == 1:
        for k,v in H.node[next_hop_px]['flows'].items():
            print 'node',k
            sum_lc_prime +=H.node[k]['lambda_c']
        sum_lc_prime = sum_lc_prime - H.node['o_client']['lambda_c']
        print 'Sum lc Prime if:', sum_lc_prime
    else:
        lf = H.successors(next_hop_px)
        for j in lf:
            sum_lc_prime+=H.node[j]['lc']
        sum_lc_prime = sum_lc_prime - H.node[o_client]['flows'][o_client]
        print "Sum_lc_prime else:",sum_lc_prime

    oq_client = o_client

    # Make call to recursion
    o_result = o_cl_yt_prime_r_x(H,T,next_hop_px,oq_client,0)

    # Execute original equation
    var1 = ((T.kn*(T.control_message + T.data_message)*o_hops) +(T.kd*T.data_message))
    if sum_lc_prime != 0: # this control any division by zero for the lambda prime calculations
        var2 = (H.node[T.server]['ls']*T.data_message*T.kd_prime)/(sum_lc_prime)
    else:
        var2 = float(0)
    var3 = (H.node[o_client]['ucx'] * o_result)


    overhead_cl_yt_c_r = var1+var2+var3

    H.node[oq_client]['o_cl_yt'] = overhead_cl_yt_c_r

    print "Overhead Result O_CL_YT: ",overhead_cl_yt_c_r

    pass


def o_cl_yt_prime_r_x(H,T,inc_src2,op_client,na):
    ####  O_CL_YT prime rx
    ''' op_client: this is the client that the latency is being calculated for
        inc_src2: is the next hop serving proxy
    '''
    stat_client_2 = op_client
    print "Entered  Overhead function *********************************************"
    print 'Static client:',stat_client_2
    print 'incoming node:', inc_src2

    n_hop = d_x_c(H,T,inc_src2,T.server)
    higher_node = d_x_c(H,T,n_hop,T.server)
    or_hops = h_a_b_c(H,T,inc_src2,n_hop)
    print "Found overhead next hop:",n_hop


    if n_hop == T.server:
        print "Entered exit clause Overhead CL-YT function"

        ocl_val_yt= (T.data_message)*(T.kd+T.kd_prime)

        return ocl_val_yt
    else:

        o_var1 = (T.kn*(T.control_message + T.data_message)*or_hops) + (2*T.kd*T.data_message)
        o_var4 = (T.kn*H.node[T.server]['ls']*T.control_message* h_a_b_c(H,T,n_hop,higher_node))/( (1-H.node[n_hop]['ucx']) * H.node[n_hop]['lc']  )

        o_yt   =  o_var1  + (H.node[n_hop]['ucx'] *o_cl_yt_prime_r_x(H,T,n_hop,stat_client_2,0)) + o_var4
        print "inside recursion overhead  yt:", o_yt

        return o_yt






def calc_client_prob(H,T):
    '''   NEW  WORKING GREAT
        This function calculates recursively from the max depth of the tree
        upwards to the server the probability values for the clients

    '''
    ty=[T.calc_tree_depth(H,T.server,w) for w in T.proxy if H.node[w]['node_type']=='proxy' ]
    level = max(ty)
    cy=[T.calc_tree_depth(H,T.server,w) for w in T.clients if H.node[w]['node_type']=='client']
    print "Client depth list", cy
    c_lev = max(cy)

    while level >0:
        for pa in T.proxy:
            if H.node[pa]['node_depth']==level and H.node[pa]['node_type']=='proxy':
                served_nodes = H.node[pa]['clients_served']
                print served_nodes
                cnt = int(0)
                num_of_suc = len(served_nodes)
                while c_lev>0:
                    for s in served_nodes:
                        if H.node[s]['node_depth']==c_lev:
                            #q= d_x_c(H,T,s,pa)
                            px_check = d_x_c(H,T,s,pa)
                            if px_check: # If a serving proxy is returned then calculate client probability
                                num_of_chil = H.successors(px_check)
                                if proxy_sharing(H, pxy=px_check):
                                    print "Calculate sharing client probability for node:",s
                                    H.node[s]['ucx'] = u_c_p_ys(H,T,s,px_check)
                                    H.node[s]['lcx'] = H.node[s]['ucx'] * sum_flows(H,T,s)

                                    edit_flows(H,T,s,H.node[s]['ucx'])
                                    update_flow(H,T,px_check) #TEST
                                    update_upstream_proxies(H,T,px_check)
                                    #sum_reqrate(H,T,px_check) #TEST
                                    print "Update Done Sharing"
                                else:
                                    print "Calculate non-sharing client probability for node:",n
                                    H.node[s]['ucx'] = u_c_p_ns(H,T,s)
                                    H.node[s]['lcx'] = H.node[s]['ucx'] * sum_flows(H,T,s)
                                    edit_flows(H,T,s,H.node[s]['ucx'])
                                    update_flow(H,T,px_check) #TEST
                                    update_upstream_proxies(H,T,px_check)
                                    #sum_reqrate(H,T,px_check) #TEST
                                    #edit_flows(H,T,n,H.node[n]['ucx'])
                                    print "Update done non-sharing"

                    c_lev = c_lev-1
        level = level -1

    pass



def cdn_processing():

    '''
        Calculates latency incurred by this client
        Stage 1:update low level forwarding nodes upstream
        Stage 2: A
        Stage 3: R
        Stage 4: R

        Parameters:
            - client, server, Graph object G|H,

        Return:
            -
    '''

    print("Latency test function")

    server = 1000
    clients = [100,200,300,400,500]
    proxy_setup = []
    inp = [1,3,5,8,13,16,18] # Proxy Nodes
    control_message = float(400) # bits
    data_message = float(800000)  #bits
    bandwidth = float(10000000)  # 10Mbps

    pxy = [int(l) for l in inp] # Converting Proxy entries to integers
    #edge_serving_proxies = [5,6,7,8,9,12] # here we specify proxies/forwarding node at the boundary of the network

    print "NEW Proxies: ",pxy
    print "Server Notice:",server
    proxy_setup.append(pxy)

    print "Proxy Setup", proxy_setup
    output = dict()
    idx = int(0)
    for pos in proxy_setup:
       idx+=int(1)
       print 'INITIAL CLIENTS', clients
       T = CDN_Tree(pos, clients, server)

       print "+++++++++++++++++++++++PROXY PLACEMENT: %s    +++++++++++++++" % str(pos)

       H = T.setup_cdn_tree()
       p = T.proxy_placement(H,clients,pos)
       T.bandwidth = bandwidth
       T.control_message = control_message
       T.data_message = data_message
       T.u_n = bandwidth/control_message
       print "Content fwding delay: ",T.u_n
       T.u_n_p = bandwidth/data_message
       print "Content storing delay ", T.u_n_p
       T.u_q = float(40)
       print "Average disk service rate ", T.u_q
       T.kd = float(1)
       T.kd_prime=float(1)
       T.kn = float(1)




    update_flow_clients(H,T)

    proxy_pruning(H,T)

    Tree_nodes = H.nodes(data=True)
    for t in Tree_nodes:
        print t


    print "All nodes:",T.all_nodes
    print "Number of nodes:",len(T.all_nodes)


    print "********************** CDN Processing *********************"

    for clients_nd in clients:
        l_cl_yt_c_r(H,T,clients_nd,T.server)
        l_cl_nt_c_r(H,T,clients_nd,T.server)
        latency_sr(H,T,clients_nd)
        o_cl_yt_c_r(H,T,clients_nd,T.server)

    print "Final Output"

    Tree_nodes = H.nodes(data=True)
    for t in Tree_nodes:
        print t


    pass



if __name__ == "__main__":
    #main()
    cdn_processing()


else:
    cdn_processing()
    print("NetworkX_graph2.py is being imported into another module")


''' PROGRAM NOTES


'''


